package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

	int maxItems = 20;
	ArrayList<FridgeItem> items;
	ArrayList<FridgeItem> expired;

	public Fridge() {
		items = new ArrayList<FridgeItem>(20);
		expired = new ArrayList<FridgeItem>(20);
	}


	@Override
	public int nItemsInFridge() {
		int nItemsInFridge = items.size();
		return nItemsInFridge;
	}

	@Override
	public int totalSize() {
		return maxItems;
	}

	@Override
	public boolean placeIn(FridgeItem item) {
		if (this.nItemsInFridge() < this.totalSize()) {
			items.add(item);
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public void takeOut(FridgeItem item) {
		if (items.contains(item)){
			items.remove(item);
		}
		else {
			throw new NoSuchElementException();
		}
	}

	@Override
	public void emptyFridge() {
		items.clear();
	}

	@Override
	public List<FridgeItem> removeExpiredFood() {
		for (FridgeItem i : items) {
			if (i.hasExpired()) {
				expired.add(i);
			}
			else {
				continue;
			}
		}
		items.removeAll(expired);
		return expired;
	}
	
}
